-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.21-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for dbsipenca
CREATE DATABASE IF NOT EXISTS `dbsipenca` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `dbsipenca`;

-- Dumping structure for table dbsipenca.barang
CREATE TABLE IF NOT EXISTS `barang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `id_satuan` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.barang: ~4 rows (approximately)
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
REPLACE INTO `barang` (`id`, `id_supplier`, `nama`, `qty`, `id_satuan`, `harga`) VALUES
	(1, 1, 'Kaca Mata Minus', 18, 1, 45000),
	(2, 1, 'Kaca Mata Baca', 35, 2, 30000),
	(3, 1, 'Kaca Mata Plus', 5, 2, 5000),
	(4, 3, 'Kaca Mata Gaya', 30, 2, 5000),
	(5, 1, 'ABCD', 0, 1, 32000);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.barang_keluar
CREATE TABLE IF NOT EXISTS `barang_keluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.barang_keluar: ~9 rows (approximately)
/*!40000 ALTER TABLE `barang_keluar` DISABLE KEYS */;
REPLACE INTO `barang_keluar` (`id`, `id_user`, `waktu`) VALUES
	(1, 1, '2020-04-28 21:47:32'),
	(2, 1, '2020-05-01 04:41:59'),
	(3, 1, '2020-05-01 04:42:55'),
	(4, 1, '2020-05-01 04:55:48'),
	(5, 1, '2020-05-01 21:41:48'),
	(6, 1, '2020-05-01 23:06:34'),
	(7, 1, '2020-05-15 21:43:08'),
	(8, 1, '2020-05-15 23:35:52'),
	(9, 1, '2022-07-17 23:14:15');
/*!40000 ALTER TABLE `barang_keluar` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.barang_keluar_detail
CREATE TABLE IF NOT EXISTS `barang_keluar_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang_keluar` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.barang_keluar_detail: ~11 rows (approximately)
/*!40000 ALTER TABLE `barang_keluar_detail` DISABLE KEYS */;
REPLACE INTO `barang_keluar_detail` (`id`, `id_barang_keluar`, `id_barang`, `qty`) VALUES
	(1, 1, 1, 5),
	(2, 2, 3, 1),
	(3, 2, 2, 3),
	(4, 3, 2, 2),
	(5, 3, 1, 1),
	(6, 4, 3, 2),
	(7, 5, 2, 870),
	(8, 6, 2, 1),
	(9, 7, 2, 2),
	(10, 8, 4, 30),
	(11, 9, 2, 2);
/*!40000 ALTER TABLE `barang_keluar_detail` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.barang_masuk
CREATE TABLE IF NOT EXISTS `barang_masuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `waktu` datetime NOT NULL DEFAULT current_timestamp(),
  `total_harga` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.barang_masuk: ~12 rows (approximately)
/*!40000 ALTER TABLE `barang_masuk` DISABLE KEYS */;
REPLACE INTO `barang_masuk` (`id`, `id_user`, `waktu`, `total_harga`) VALUES
	(1, 1, '2020-04-28 21:31:48', 810000),
	(4, 1, '2020-04-30 23:09:47', 65000),
	(5, 1, '2020-04-30 23:13:58', 90000),
	(6, 1, '2020-04-30 23:22:47', 5000),
	(7, 1, '2020-04-30 23:36:31', 50000),
	(8, 1, '2020-04-30 23:37:06', 35000),
	(9, 1, '2020-04-30 23:37:44', 105000),
	(10, 1, '2020-05-01 01:28:29', 45000),
	(11, 1, '2020-05-01 21:35:07', 27000000),
	(12, 1, '2020-05-15 21:29:37', 180000),
	(13, 1, '2020-05-15 23:30:44', 300000),
	(14, 1, '2022-07-17 23:13:22', 180000);
/*!40000 ALTER TABLE `barang_masuk` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.barang_masuk_detail
CREATE TABLE IF NOT EXISTS `barang_masuk_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang_masuk` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.barang_masuk_detail: ~16 rows (approximately)
/*!40000 ALTER TABLE `barang_masuk_detail` DISABLE KEYS */;
REPLACE INTO `barang_masuk_detail` (`id`, `id_barang_masuk`, `id_barang`, `qty`, `subtotal`) VALUES
	(1, 1, 1, 10, 450000),
	(4, 4, 1, 1, 45000),
	(5, 4, 3, 4, 20000),
	(6, 5, 1, 2, 90000),
	(7, 6, 3, 1, 5000),
	(8, 7, 1, 1, 45000),
	(9, 7, 3, 1, 5000),
	(10, 8, 2, 1, 30000),
	(11, 8, 3, 1, 5000),
	(12, 9, 2, 2, 60000),
	(13, 9, 1, 1, 45000),
	(14, 10, 1, 1, 45000),
	(15, 11, 2, 900, 27000000),
	(16, 12, 1, 4, 180000),
	(17, 13, 4, 60, 300000),
	(18, 14, 1, 4, 180000);
/*!40000 ALTER TABLE `barang_masuk_detail` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.keranjang_keluar
CREATE TABLE IF NOT EXISTS `keranjang_keluar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.keranjang_keluar: ~0 rows (approximately)
/*!40000 ALTER TABLE `keranjang_keluar` DISABLE KEYS */;
/*!40000 ALTER TABLE `keranjang_keluar` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.keranjang_masuk
CREATE TABLE IF NOT EXISTS `keranjang_masuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.keranjang_masuk: ~0 rows (approximately)
/*!40000 ALTER TABLE `keranjang_masuk` DISABLE KEYS */;
/*!40000 ALTER TABLE `keranjang_masuk` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.satuan
CREATE TABLE IF NOT EXISTS `satuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `status` enum('valid','invalid') NOT NULL DEFAULT 'valid',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.satuan: ~6 rows (approximately)
/*!40000 ALTER TABLE `satuan` DISABLE KEYS */;
REPLACE INTO `satuan` (`id`, `nama`, `status`) VALUES
	(1, 'pcs', 'valid'),
	(2, 'unit', 'valid'),
	(3, 'box', 'valid'),
	(4, '-', 'invalid'),
	(5, '-', 'invalid'),
	(6, '-', 'valid');
/*!40000 ALTER TABLE `satuan` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefon` varchar(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `status` enum('aktif','non-aktif') NOT NULL DEFAULT 'aktif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.supplier: ~6 rows (approximately)
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
REPLACE INTO `supplier` (`id`, `nama`, `email`, `telefon`, `alamat`, `status`) VALUES
	(1, 'Kacong Banjir Pole', 'kacong@gmail.com', '0833747747', 'Bangkalan', 'aktif'),
	(2, 'PT Maju Mundur', 'maju.mundur@korporat.com', '08484844332', 'Jl. Medokan Asri Barat No. 42', 'aktif'),
	(3, 'Depot Sunu', 'sunu.ilham@gmail.com', '09944233334', 'Sidoarjo', 'aktif'),
	(4, 'PT Majuo Kabeh Sak Perumahan', 'looos@gmail.com', '9099944423', 'Kediri', 'aktif'),
	(5, 'PT Besok Libur', 'tanggal.kecepit@gmail.com', '2888998894', 'Bekasi', 'aktif'),
	(6, 'PT Pencari Makna Hidup', 'pmh@gmail.com', '84894848444', 'Gunung Lawas', 'aktif');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;

-- Dumping structure for table dbsipenca.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `telefon` varchar(15) NOT NULL,
  `ktp` varchar(30) NOT NULL,
  `role` enum('admin','manajer','keuangan','kasir') NOT NULL DEFAULT 'kasir',
  `status` enum('aktif','non-aktif') NOT NULL DEFAULT 'aktif',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table dbsipenca.user: ~4 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
REPLACE INTO `user` (`id`, `nama`, `email`, `password`, `telefon`, `ktp`, `role`, `status`) VALUES
	(1, 'Admin', 'admin@sipenca.com', '$2y$10$UBpSa8Hl07HyfR5CF.RlvOQDmsh6X/aKJPUriqmv99pxjlMwBerv.', '084554433445', '17081010000', 'admin', 'aktif'),
	(2, 'Dela', 'dela@sipenca.com', '$2y$10$UBpSa8Hl07HyfR5CF.RlvOQDmsh6X/aKJPUriqmv99pxjlMwBerv.', '08544433444', '17081010045', 'manajer', 'aktif'),
	(4, 'Sudiarti 1', 'dela1@sipenca.com', '$2y$10$UBpSa8Hl07HyfR5CF.RlvOQDmsh6X/aKJPUriqmv99pxjlMwBerv.', '08544433444', '17081010045', 'keuangan', 'aktif'),
	(5, 'Sudiarti 2', 'dela2@sipenca.com', '$2y$10$UBpSa8Hl07HyfR5CF.RlvOQDmsh6X/aKJPUriqmv99pxjlMwBerv.', '08544433444', '17081010045', 'kasir', 'aktif');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for trigger dbsipenca.kurangi_barang
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `kurangi_barang` BEFORE INSERT ON `barang_keluar_detail` FOR EACH ROW BEGIN
	UPDATE barang
	SET qty = qty - NEW.qty
	WHERE id = NEW.id_barang;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger dbsipenca.subtotal_keranjang_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `subtotal_keranjang_masuk` BEFORE INSERT ON `keranjang_masuk` FOR EACH ROW BEGIN
	# ---- Hitung Subtotal ----
	DECLARE harga_barang INT DEFAULT 0;
	SET harga_barang = (SELECT harga FROM barang WHERE id = NEW.id_barang LIMIT 1);

	SET NEW.subtotal = NEW.qty * harga_barang;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger dbsipenca.tambah_barang
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `tambah_barang` AFTER INSERT ON `barang_masuk_detail` FOR EACH ROW UPDATE barang
SET qty = qty + NEW.qty
WHERE id = NEW.id_barang//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

-- Dumping structure for trigger dbsipenca.total_harga_masuk
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ZERO_IN_DATE,NO_ZERO_DATE,NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `total_harga_masuk` AFTER INSERT ON `barang_masuk_detail` FOR EACH ROW BEGIN
	UPDATE barang_masuk
	SET total_harga = total_harga + NEW.subtotal
	WHERE id = NEW.id_barang_masuk;
END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
